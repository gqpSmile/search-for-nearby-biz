// page/mainPackage//pages/share/nearbyBizList/nearbyBizList.js
const app = getApp()
var http = require('../../utils/request.js');


Page({

    /**
     * 页面的初始数据
     */
    data: {
        postion: true, //地理位置是否授权
        nearbyListData: [],
        p: 1,
        page_size: 13,
        is_empty: false,
        hasMoreData: true,
        hiddenLoading: true,
        lng: 0.00, //经度
        lat: 0.00, //纬度
        hideAuthBtn: true,


        list: [ //测试数据 仅供参数
            {
                address: "广东省深圳市南山区南山街道...",
                avatar: "",
                business_id: 6,
                distance: "14.50",
                lat: "22.532598",
                lng: "113.913766",
                shop_name: "001用户体验店",
                unit: "km"
            },
            {
                address: "广东省深圳市宝安区...",
                avatar: "",
                business_id: 33,
                distance: "18.67",
                lat: "22.580897",
                lng: "113.883902",
                shop_name: "小不点用户体验店",
                unit: "km"
            }
        ]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        // 调用wx.getLocation系统API, 获取并设置当前位置经纬度
        this.check_Authorization();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {
        var that = this;
        if (that.data.hasMoreData) {
            that.getnearbyList(false);
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },

    /**
     * 获取用户当前设置
     */
    check_Authorization: function() {
        var that = this;
        wx.getSetting({
            success: (res) => {
                if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] == true) {
                    that.setData({
                        postion: true,
                    })
                    that.getLocation();
                } else {
                    that.setData({
                        postion: false
                    })
                    that.getLocation();
                }
            },
            fail(res) {
            }
        })
    },

    /**
     * 调用wx.getLocation系统API,获取并设置当前位置经纬度
     */
    getLocation: function() {
        var that = this;
        //获取位置
        wx.getLocation({
            type: 'gcj02', //默认为 wgs84 返回 gps 坐标，gcj02 返回可用于wx.openLocation的坐标
            success: function(res) {
                that.setData({
                    postion: true,
                })
                that.setData({
                    lng: res.longitude,
                    lat: res.latitude,
                })

                // 获取附近商家列表
                that.getnearbyList(true);
            },
            fail: function(e) {
                that.setData({
                    hideAuthBtn: false
                })
            }
        });
    },

    /**
     * 手动授权
     */
    handler: function(e) {
        var that = this;
        if (!e.detail.authSetting['scope.userLocation']) {
            that.setData({
                postion: false
            })
        } else {
            that.setData({
                postion: true,
            })
        }
    },

    /*
     * 获取附近商家列表
     */
    getnearbyList: function(type = true) {
        var that = this;

        // 模拟数据
        this.setData({
            nearbyListData: this.data.list
        })

        // -------------以下为请求接口处理代码-------------------
        // var params = 'p=' + that.data.p + '&page_size=' + that.data.page_size + '&lng=' + that.data.lng + '&lat=' + that.data.lat;

        // http.getReq("Nearby/nearbyList?" + params, function(res) {
        //     if (res.code != 200) {
        //         wx.showToast({
        //             title: res.msg,
        //             mask: false
        //         })
        //         return
        //     }
        //     var datas = res.data.list;
        //     var dataList = that.data.nearbyListData.concat(datas);

        //     that.setData({
        //         nearbyListData: dataList,
        //         hiddenLoading: true,
        //         p: that.data.p + 1
        //     })

        //     if ((datas.length != that.data.page_size) && (datas.length > 0)) {
        //         //全部加载完成，显示没有更多数据
        //         that.setData({
        //             hasMoreData: false,
        //             hiddenLoading: false
        //         });
        //     }

        //     //  处理显示加载中
        //     if (datas.length == 0) {
        //         that.setData({
        //             hiddenLoading: false,
        //             hasMoreData: false,
        //         });
        //     }
        //     if (dataList.length == 0) {
        //         that.setData({
        //             is_empty: true,
        //         });
        //     }
        // }, type)
    },

    /**
     * 查看具体商家信息
     */
    distributionBIZTap: function(e) {
        var that = this;
        //商家当前id
        var bizid = e.currentTarget.dataset.bizid;
        this.data.nearbyListData.forEach((item, i) => {
            if (item.business_id == bizid) {
                // 跳转目标位置
                that.openLocation(item);
            }
        })
    },

    /**
     * 使用微信内置地图查看商家位置
     */
    openLocation: function(item) {
        var that = this;
        wx.getLocation({ //获取当前经纬度
            type: 'gcj02', //返回可以用于wx.openLocation的经纬度，官方提示bug: iOS 6.3.30 type 参数不生效，只会返回 wgs84 类型的坐标信息  
            success: function(res) {
                wx.openLocation({ //​使用微信内置地图查看位置。
                    latitude: Number(item.lat), //要去的纬度-地址
                    longitude: Number(item.lng), //要去的经度-地址
                    name: item.shop_name, //店铺名
                    address: item.address //详细地址
                })
            }
        })
    },

})